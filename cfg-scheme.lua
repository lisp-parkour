-- SPDX-License-Identifier: GPL-3.0-or-later
-- © 2020 Georgi Kirilov

local l = require'lpeg'
local P, S, R, Cc = l.P, l.S, l.R, l.Cc

local let_likes = (P'match-' + 'splicing-')^-1 * 'let' * (P'rec' + '*')^-1 * (P'-' * (P'syntax' + 'syntaxes' + 'values'))^-1 * -P(1)
local forfold_likes = 'for' * P'*'^-1 * '/' * (P'fold' + 'lists')

local lispwords = {
	-- If none of the string keys matched, this pattern is attempted
	[0] = ('define' * ('*' + '-' * P(1)^1)^-1 * -P(1) +
		(P'call-'^-1 * 'with-') * P(1)^1 +
		let_likes) * Cc(1) +
		forfold_likes * Cc(2),

	['begin'] = -1,
	['case-lambda'] = -1,
	['cond'] = -1,
	['delay'] = -1,
	['match-lambda'] = -1,
	['match-lambda*'] = -1,
	['sequence'] = -1,
	['syntax-parser'] = -1,

	['case'] = 1,
	['compile-time-case'] = 1,
	['eval-when'] = 1,
	['lambda'] = 1,
	['lambda*'] = 1,
	['library'] = 1,
	['local-even?'] = 1,
	['local-odd?'] = 1,
	['match'] = 1,
	['module'] = 1,
	['parameterize'] = 1,
	['struct'] = 1,
	['syntax-parameterize'] = 1,
	['syntax-parse'] = 1,
	['syntax-rules'] = 1,
	['unless'] = 1,
	['when'] = 1,
	['λ'] = 1,

	['do'] = 2,
	['do*'] = 2,
	['syntax-case'] = 2,
	['with-slots'] = 2,
}

local _1 = {1, false}
local _2 = {2, false}

local for_likes = 'for' * P'*'^-1 * ('/' * P(1 - S':/')^1)^-1 * P':'^-1 * -P(1)

local squarewords = {
	-- If none of the string keys matched, this pattern is attempted
	[0] = for_likes * Cc(_2) + let_likes * Cc(_1),

	['case-lambda'] = -1,
	['cond'] = -1,
	['match-lambda'] = -1,
	['match-lambda*'] = -1,

	['case'] = -2,
	['define/match'] = -2,
	['match'] = -2,
	['match*'] = -2,
	['match/values'] = -2,
	['syntax-parse'] = -2,
	['syntax-rules'] = -2,

	['syntax-case'] = -3,

	['syntax-case*'] = -4,

	['local'] = 1,

	['fluid-let'] = _1,
	['parameterize'] = _1,
	['parameterize*'] = _1,
	['syntax-parameterize*'] = _1,
	['with-handlers'] = _1,
	['with-syntax'] = _1,

	['letrec-syntaxes+values'] = _2,
}

-- TODO: add more prefixes
local macro_prefix =
	P'#'^-1 * (P',@' + S",`'") +
	P'#' * (
		S'us' * (P'8' + '16' + '32' + '64') + P'f' * (P'32' + '64') +
		R'09'^0 * 'vu8' +
		R'09'^1 +
		'hash' * P('eq' * P'v'^-1 + 'alw')^-1 +
		'ci' + 'rx' + 'px' +
		S's&;'
		)^-1

-- XXX: weak_prefix can be added for some Schemes

local delimiters = {
	['('] = ')',
	['{'] = '}',
	['['] = ']',
	['"'] = '"',
	['|'] = '|',
	[';']  = '\n',
	['#|'] = '|#',
}

return {
	prefix = macro_prefix,
	opposite = delimiters,
	lispwords = lispwords,
	squarewords = squarewords,
}
