Parkour is a Lua library for structured editing of Lisp S-expressions, meant to be used in text editors.

It is licensed under the [GNU General Public License (GPL) version 3](https://www.gnu.org/licenses/gpl-3.0.html), or any later version.  
The code is compatible with Lua 5.1+ and LuaJIT.  
The only external dependency is [LPeg](https://www.inf.puc-rio.br/~roberto/lpeg/).

Editors supported so far:

- [Vis](https://github.com/martanne/vis) - via [vis-parkour](https://repo.or.cz/vis-parkour.git)
- [Textadept](https://github.com/orbitalquark/textadept) - via [ta-parkour](https://repo.or.cz/ta-parkour.git)
- [Howl](https://github.com/howl-editor/howl) - via [howl-parkour](https://repo.or.cz/howl-parkour.git)

# Embedding

Parkour needs the following callback functions to be implemented in terms of editor-specific APIs:

- `read(pos, len)`
- `write(pos, str)`
- `delete(pos, len)`
- `eol_at(pos)`
- `bol_at(pos)`

They are supposed to work on the currently opened file.
Its name is not explicitly used anywhere, you just need to ensure
that it is in a language that Parkour understands.

`pos` and `len` are measured _in bytes_. Still, having multi-byte characters
in identifiers and comment words is fine. As long as separators/delimiters are
single-byte characters, Parkour will simply ignore what's in between.
It can afford to because its commands only return positions at word or list boundaries.

    local init, supported = table.unpack(require'parkour')

    event.subscribe(FILE_OPENED, function(file)
        if not supported[file.type] then return end
        # Definitions of the callback functions go here.
        local pk = init(file.type, read, write, delete, eol_at, bol_at)
    end)

`supported` is a table with the names of all [supported Lisp dialects](init.lua#l4) as keys.  
`init()` returns a table with instances of
[`walker`](walker.lua),
[`edit`](edit.lua),
[`input`](input.lua), and
[`parser`](parser.lua)
objects, initialized to work on a particular file.

Most of those objects' methods are almost ready for direct use.  
`walker` and `edit` methods will usually be bound via keymaps, while
`input` is best handled by some sort of keypress/input event handler.

The methods usually take a `range` argument (a table) which is an abstraction over cursor/selection.  
If `range.start == range.finish`, it represents an "I-beam" cursor, like in GUI editors, or "the point" in Emacs.  
If `range.finish > range.start`, then it's either a block cursor, like in vim, or a selection.  
Some methods take an extra `pos` argument, when the "direction" of a selection is important.
(`pos` should be equal to either `range.start` or `range.finish`)  
The return value of a method is the new position the cursor should be put at.
