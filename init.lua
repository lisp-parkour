-- SPDX-License-Identifier: GPL-3.0-or-later
-- © 2020 Georgi Kirilov

local supported = {lisp = true, scheme = true, clojure = true, fennel = true}
local cwd = ...

local walker = require(cwd..".walker")
local parser = require(cwd..".parser")
local input = require(cwd..".input")
local edit = require(cwd..".edit")
local node = require(cwd..".node")

local function init(dialect, read, write, delete, eol_at, bol_at)
	local c = require(cwd..".cfg-"..dialect)
	local n = node.new(read)
	local p, d1, D2 = parser.new(c, n, read)
	local w = walker.new(p, eol_at, bol_at)
	local e = edit.new(p, w, c.squarewords, write, delete, eol_at, bol_at)
	local i = input.new(p, d1, D2, w, e, write, eol_at, bol_at)
	return {parser = p, walker = w, input = i, edit = e, lispwords = c.lispwords}
end

return {init, supported}
