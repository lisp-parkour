-- SPDX-License-Identifier: GPL-3.0-or-later
-- © 2020 Georgi Kirilov

local M = {}

local function startof(node) return node.start end
local function finishof(node) return node.finish end

local function make_insert(parser, d1, D2, walker, edit, write, eol_at, bol_at)

	local function open(range, seek, opening, auto_square)
		local escaped = walker.escaped_at(range)
		if escaped and range.start > escaped.start then
			if opening == escaped.d then
				seek(escaped.finish + 1)
				return true
			elseif escaped.is_char then
				return true
			end
			write(range.start, opening)
			seek(range.start + #opening)
			parser.tree.rewind(escaped.start)
		else
			local _, parent = walker.sexp_at(range)
			-- TODO: make it work even when before the first existing element on the electric line:
			local on_electric_line = parent.finish and parent.finish > eol_at(range.finish)
				and not parent.find_after(range, function(t) return t.indent end)
			local newpos = edit.insert_pair(range, opening, auto_square)
			seek(edit.refmt_at(parent, {start = newpos, finish = newpos}, on_electric_line))
		end
		return true
	end

	local function close(range, seek, kind)
		local pos = range.start
		local escaped = walker.escaped_at(range)
		if escaped then
			write(pos, kind)
			seek(pos + 1)
			parser.tree.rewind(escaped.start)
		else
			local _, parent = walker.sexp_at(range)
			if parent and parent.is_list then
				range = {start = parent.finish, finish = parent.finish}
				local newpos = edit.refmt_at(parent, range)
				seek(newpos + 1)
				parser.tree.rewind(parent.start)
			end
		end
		return true
	end

	local function spacekey(range, seek, char)
		local sexp, parent = walker.sexp_at(range)
		-- if parent[#parent + 1] is nil, we are at EOF
		if not parent.is_root or parent.is_parsed(range.start) or parent[#parent + 1] then
			parser.tree.rewind(parent.start or range.start)
		end
		if not parent.is_list or parent.is_empty then return end
		local after_last = parent[#parent].finish and range.start > parent[#parent].finish
		local bol = bol_at(range.start)
		local eol = eol_at(range.start)
		local prev = parent.before(range.start, finishof)
		local nxt = parent.after(range.start, startof)
		local on_empty_line = not sexp and ((prev and prev.finish or parent.start) < bol)
			and ((nxt and nxt.start or parent.finish) > eol)
		local at_eol = range.start == eol
		local newpos
		if not on_empty_line then
			local _
			write(range.start, char)
			newpos = range.start + #char
			_, parent = walker.sexp_at(range)
		else
			newpos = bol - 1
		end
		if not at_eol or on_empty_line then
			newpos = edit.refmt_at(parent, {start = newpos, finish = newpos}, after_last and not on_empty_line)
			if on_empty_line then
				parser.tree.rewind(newpos)
				write(newpos, char)
				newpos = newpos + #char
			end
		end
		seek(newpos)
		return true
	end

	local function enterkey(range, seek)
		local _, parent = walker.sexp_at(range)
		local newpos = edit.newline(parent, range)
		if newpos then
			seek(newpos)
			return true
		end
	end

	local comment_start = parser.opposite["\n"]

	local function open_comment(range, seek, char)
		local escaped = walker.escaped_at(range)
		if escaped and escaped.is_char then return true end
		local datum, sexp, parent, descend_into_next_comment, stay_out
		if not (escaped and range.start > escaped.start) then
			sexp, parent = walker.sexp_at(range, true)
			datum = sexp and sexp.p and sexp.p:find("^#") and range.start == sexp.start + 1
			local _, next_safe = walker.next_finish_wrapped(range)
			local eol = eol_at(range.start)  -- XXX: must be before the write. The vis eol_at wrapper is leaky
			local bol = bol_at(range.start)  -- XXX: must be before the write. The vis eol_at wrapper is leaky
			if next_safe and not datum then
				write(next_safe, parser.opposite[char])
			end
			if not datum then
				local prev = parent.before(range.start, finishof)
				local nxt = parent.after(range.start, startof)
				local pstart = parent.start or 0
				local no_sexp_to_the_left =
					not prev and (pstart < bol or parent.is_root) or
					sexp and (sexp.indent and range.start == sexp.start or sexp.is_line_comment) or
					not sexp and prev and prev.finish < bol
				local comment_out_at =
					(not sexp or sexp.is_line_comment) and nxt and nxt.finish < eol and nxt.start or
					sexp and range.start <= sexp.finish and sexp.finish < eol and range.start
				if no_sexp_to_the_left then
					if sexp and sexp.is_line_comment and range.start == sexp.start then
						descend_into_next_comment = sexp.start + #sexp.d
					else
						if nxt and nxt.is_line_comment and nxt.start < eol then
							descend_into_next_comment = nxt.start + #nxt.d
						else
							char = char:rep(parent.is_root and 3 or 2) .. " "
						end
					end
				else
					if (not sexp or range.start == sexp.finish + 1) and
						nxt and nxt.is_line_comment and not nxt.indent then
						descend_into_next_comment = nxt.start + #nxt.d
					else
						local comment_column = 40
						local last_col = range.start - bol + 1
						local padding = math.max(1, comment_column - last_col)
						char = string.rep(" ", padding)..char
					end
				end
				if not descend_into_next_comment and comment_out_at then
					stay_out = comment_out_at
				end
			end
		end
		if stay_out then
			range.start = stay_out
		end
		if not descend_into_next_comment then
			write(range.start, char)
		end
		-- if parent[#parent + 1] is nil, we are at EOF
		if not parent or not parent.is_root or parent.is_parsed(range.start) or parent[#parent + 1] then
			parser.tree.rewind(range.start - (datum and 1 or 0))
		end
		if descend_into_next_comment then
			range.start = descend_into_next_comment
		elseif not stay_out then
			range.start = range.start + #char
		end
		range.finish = range.start
		if not descend_into_next_comment and not escaped and not parent.is_root then
			range.start = edit.refmt_at(parent, range)
		end
		seek(range.start)
		return true
	end

	return function(range, seek, char, auto_square)
		local handler =
			d1:match(char) and open or
			char == '"' and open or
			parser.opposite["|"] and char == "|" and open or
			D2:match(char) and close or
			char == " " and spacekey or
			char == "\n" and enterkey or
			char == comment_start and open_comment
		if handler then
			return handler(range, seek, char, auto_square)
		else
			if parser.tree.is_parsed(range.start) then
				parser.tree.rewind(range.start)
			end
		end
	end
end

function M.new(parser, d1, D2, walker, edit, write, eol_at, bol_at)
	return {
		insert = make_insert(parser, d1, D2, walker, edit, write, eol_at, bol_at),
	}
end

return M
