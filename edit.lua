-- SPDX-License-Identifier: GPL-3.0-or-later
-- © 2020 Georgi Kirilov

local M = {}

-- XXX: in Lua 5.2 unpack() was moved into table
local unpack = table.unpack or unpack

local function startof(node) return node.start end
local function finishof(node) return node.finish end
local function is_comment(node) return node.is_comment end

local function normalize_spacing(start, delta, list)
	if delta > 0 then
		table.insert(list, {start + delta, -delta})
	elseif delta < 0 then
		table.insert(list, {start, -delta})
	end
	return delta
end

local function is_lone_prefix(node)
	return node and node.p and (#node.p == node.finish - node.start + 1)
end

local function adjust_bracket_p(indices, nodes, range)
	local parent = nodes[#nodes - 1]
	if not parent then return true end
	local nth = indices[#indices]
	if not nth then
		local _, nxt = parent.find_after(range, function() return true end)
		nth = nxt or #parent + 1
	end
	local sexp = parent[nth] or parent[#parent]
	local distance = nth - (sexp and range.start == sexp.finish + 1 and 0 or 1)
	local rule = parent[1] and parent[1].auto_square
	if type(rule) == "number" then
		if rule < 0 then
			return -rule > distance
		elseif rule > 0 then
			-- XXX: the "[" check is because in Fennel fn has optional name
			return rule < distance or sexp.d == "["
		end
	elseif not rule then
		local grandparent, pnth = nodes[#nodes - 2], indices[#indices - 1]
		if not grandparent then return true end
		rule = grandparent[1] and grandparent[1].auto_square
		if not rule or type(rule) ~= "table" then return true end
		local _, first_list_arg = grandparent.find_after(grandparent[1], function(t) return t.is_list end)
		local first_nonlist_uncle = first_list_arg - 1
		-- TODO: handle negative rule[1] and rule[2] (just for consistency with the scalar rules)
		-- first_nonlist_uncle is checked so we can handle both let and named let:
		if (not rule[1] or first_nonlist_uncle + rule[1] >= pnth) then
			if (not rule[2] or rule[2] >= distance + 1) then
				if parent[distance] then
					-- allows mixing [clauses] with #:when conditions in Racket for loops:
					if parent[distance].is_list then
						return false
					end
				else
					return false
				end
			end
		end
	end
	return true
end

function M.new(parser, walker, squarewords, write, delete, eol_at, bol_at)

	local function refmt_list(parent, base_indent, padj, deltas, keep_electric_space)
		local adj = 0
		local op_adj = 0
		local indent_adj = 0
		local pstart = parent.start + (parent.p and #parent.p or 0) + #parent.d
		if parent.is_empty then
			normalize_spacing(pstart, parent.finish - pstart, deltas)
			return deltas
		end
		local last_distinguished = parent[1].last_distinguished
		local first_noncomment_argument = 1
		if #parent > 2 and parent[1].is_list and #parent[1] > 1 then
			local op = parent[1]
			local nearest_indented = op[#op].indent and op[#op] or walker.indented_before(op[#op])
			indent_adj = nearest_indented.start - pstart - nearest_indented.indent + 1
		end
		for i, s in ipairs(parent) do
			adj = s.indent and 0 or adj
			if last_distinguished then
				-- do not let early comments influence the indentation of real expressions:
				if i <= last_distinguished and s.is_comment then
					last_distinguished = last_distinguished + 1
				end
				-- s is the name of a named let. account for it:
				if i == last_distinguished + 1 and not s.is_list and parent[1].text == "let" then
					last_distinguished = last_distinguished + 1
				end
				-- do not let early comments influence the indentation of real expressions:
			elseif i <= first_noncomment_argument + 1 and s.is_comment then
				first_noncomment_argument = first_noncomment_argument + 1
			end
			local has_eol = s.is_line_comment
			local prev = parent[i - 1]
			if not s.indent and not (prev and (s.d == "|" or prev.d == "|")) and not has_eol then
				local off = prev and prev.finish + 1 or pstart
				adj = adj + normalize_spacing(off, s.start - off - (prev and 1 or 0), deltas)
			end
			local nxt = parent[i + 1]
			local is_last = not nxt
			if (is_last and not keep_electric_space) or nxt and nxt.indent then
				local off = is_last and parent.finish - (has_eol and i < #parent and 1 or 0) or nxt.start - nxt.indent - 1
				local finish = s.finish + (has_eol and i < #parent and 0 or 1)
				normalize_spacing(finish, off - finish, deltas)
			end
			if s.indent then
				local delta = base_indent - s.indent
				local firstarg_delta = base_indent + parent[1].finish + 2 - pstart - s.indent - op_adj - (indent_adj or 0)
				if last_distinguished and i > 1 then
					delta = delta + 1
					if i - 1 <= last_distinguished then
						if parent[first_noncomment_argument + 1].indent then
							delta = delta + 2
						else
							delta = firstarg_delta
						end
					elseif last_distinguished < 0
						and i > first_noncomment_argument + 1 and not parent[first_noncomment_argument + 1].indent then
						delta = firstarg_delta
					end
				elseif i == 1 then
					-- remove leading space
					delta = -(s.start - pstart)
				elseif                               -- align further arguments below the second one
					parent.d == "("              -- [] and {} contain data, so no arguments
					and not parent[1].is_string  -- a string can't have arguments
					--and not parent[1].is_list  -- wrong, GNU Emacs compatible behaviour
				then
					if i > first_noncomment_argument + 1 and not parent[first_noncomment_argument + 1].indent then
						delta = firstarg_delta
					end
					if delta < 0 then delta = math.max(-s.indent, delta) end
				end
				if delta ~= 0 then
					table.insert(deltas, {s.start, delta})
				end
				adj = adj - delta
			end
			if i == 1 then
				op_adj = adj
			end
			if s.is_list then
				local nearest_indented = s.indent and s or walker.indented_before(s)
				local parent_column = nearest_indented and
					nearest_indented.indent + (s.start + (s.p and #s.p or 0) + #s.d) - nearest_indented.start
					or base_indent + (s.start + (s.p and #s.p or 0) + #s.d) - pstart
				refmt_list(s, parent_column - adj, padj + adj, deltas, keep_electric_space)
			end
		end
		return deltas
	end

	local function path_from(range)
		local _, parent_at, m = walker.sexp_at(range, true)
		local path
		if parent_at[m] then
			path = walker.sexp_path(parent_at[m])
			local base = parent_at[m]
			if range.finish == base.finish + 1 then
				path.after_finish = 0  -- lists can end up with a different length
			else
				path.after_start = range.start - base.start
			end
		else
			local s = parent_at[#parent_at]
			if not s or range.start >= s.finish + 1 + (s.is_line_comment and 1 or 0) then
				path = walker.sexp_path(parent_at)
				path.at_pfinish = true
			else
				local prev, n = parent_at.before(range.start, finishof)
				if prev then
					path = walker.sexp_path(prev)
					local nxt = parent_at[n + 1]
					path.after_finish = nxt and not nxt.indent and 1 or range.finish - prev.finish - 1
				else
					path = walker.sexp_path(parent_at[1])
					path.after_start = 0
				end
			end
		end
		return path
	end

	local function pos_from(path, range)
		local sexp, parentng, n = walker.goto_path(path)
		if sexp then
			if path.after_finish then
				local nxt = n and parentng[n + 1]
				local max = nxt and nxt.start - sexp.finish - 1
				return sexp.finish + 1 - (range.finish - range.start) +
					(max and math.min(path.after_finish, max) or path.after_finish)
			elseif path.at_pfinish then
				return sexp.finish
			else
				return sexp.start + path.after_start
			end
		end
	end

	local function refmt_at(scope, range, keep_electric_space)
		if not range or not scope or scope.is_root or (scope.finish - scope.start < 2) then return range.start end
		local parent = walker.sexp_at(scope, true)
		if not (parent and parent.is_list) then return range.start end
		local path = path_from(range)
		local indented_parent = parent.indent and parent or parent.is_list and walker.indented_before(parent)
		local parent_column = indented_parent and (indented_parent.indent + parent.start - indented_parent.start +
			(parent.p and #parent.p or 0) + #parent.d) or
			0
		local deltas = refmt_list(parent, parent_column, 0, {}, keep_electric_space)
		table.sort(deltas, function(d1, d2) return d1[1] > d2[1] end)
		for _, pair in ipairs(deltas) do
			local offset, delta = unpack(pair)
			if delta > 0 then
				write(offset, string.rep(" ", delta))
			elseif delta < 0 then
				delete(offset + delta, -delta)
			end
		end
		parser.tree.rewind(parent.start or 0)
		return path and pos_from(path, range) or range.start
	end

	local function splice(pos, sexps, skip, backwards, action)
		local spliced
		local sexp = walker.sexp_at(skip, true)
		local start = sexp.start + (sexp.p and #sexp.p or 0)
		-- XXX: don't splice empty line comments _yet_. See _join_or_splice
		local tosplice = action.splice or action.wrap or not sexp.is_line_comment and sexp.is_empty
		local opening = (sexp.p or "")..sexp.d
		local closing = parser.opposite[sexp.d]
		local real_start = tosplice and sexp.start or start + #sexp.d
		local splice_closing = not sexp.is_line_comment or sexp.is_empty
		local real_finish = sexp.finish + 1 - (tosplice and splice_closing and 0 or #closing)
		local first = backwards and
			{start = real_start, finish = math.max(pos, start + #sexp.d)} or
			{start = real_start, finish = sexp.is_empty and pos or start + #sexp.d}
		local second = backwards and
			{start = sexp.is_empty and pos or sexp.finish + 1 - #closing, finish = real_finish} or
			{start = math.min(pos, sexp.finish + 1 - #closing), finish = real_finish}
		action.func(second)
		action.func(first)
		spliced = tosplice
		if action.kill then
			local ndeleted = first.finish - first.start + second.finish - second.start
			if ndeleted > 0 then
				sexps.rewind(sexp.start)
			end
		end
		return first.finish - first.start, spliced, opening, closing
	end


	-- This function handles some whitespace-only deletion corner cases magically.
	-- It does so by
	-- a) extending or shrinking the range that is to be deleted
	-- b) returning an integer to trick pick_out to glide the cursor, but calling refmt to restore the deleted spaces
	-- c) both a) and b)
	local function delete_whitespace_only(range, pos)
		local node, parent = walker.sexp_at(range)
		if node or not parent.is_list then return end
		local prev = parent.before(pos, finishof)
		local nxt = parent.after(pos, startof)
		if prev and prev.finish >= range.start or nxt and nxt.start < range.finish then return end
		local backwards = pos == range.finish
		local adj = 0
		local eol = eol_at(pos)
		local on_empty_line = not nxt or eol and nxt.start > eol
		local empty_line_after_comment = prev and prev.is_line_comment
			and on_empty_line
		if nxt and nxt.indent then
			if backwards and prev then
				-- join current line with the previous one, unless the latter is a line comment
				range.start = prev.finish + (empty_line_after_comment and 0 or 1)
				if not prev.d then
					range.finish = range.start + 1
					adj = (on_empty_line and 0 or 1)
				end
			else
				adj = nxt.start - range.finish
			end
			return adj
		end
		if not nxt and prev then
			-- clean up trailing whitespace (e.g. electric RET)
			range.start = prev.finish + 1
			range.finish = parent.finish
		elseif nxt and nxt.start == range.finish and (not prev or prev.finish + 1 == range.start) then
			if prev and prev.d or nxt and nxt.d and not is_lone_prefix(prev) then
				-- don't delete spaces near delimiters, just slide the cursor:
				if backwards then
					range.finish = range.start
				else
					range.start = range.finish
				end
			end
			return adj
		end
	end

	local function big_enough_parent(pos1, pos2)
		-- since the pos1-pos2 range can cross list boundaries, find which list contains both pos1 and pos2
		local _, p1, p2
		_, p1 = walker.sexp_at({start = pos1, finish = pos1}, true)
		if p1.is_root or not (p1.start < pos1 and p1.finish > pos2) then
			_, p2 = walker.sexp_at({start = pos2, finish = pos2}, true)
		end
		return p2 and not p2.is_root and p2 or p1
	end

	local function extend_overlap(range)
		local rnode = walker.sexp_at({start = range.finish, finish = range.finish})
		if rnode and rnode.p and rnode.p:find";"
			and range.finish > rnode.start and range.finish < rnode.start + #rnode.p then
			return {start = range.start, finish = rnode.start + #rnode.p}
		end
		return range
	end

	local function contains(range, pos)
		return pos <= range.finish and pos >= range.start
	end

	local function within(pos, start, finish)
		return finish >= pos and start <= pos
	end

	-- unbalanced_delimiters() for lines
	-- Like with line comments, which are delimited by semicolons and a newline,
	-- lines are delimited by (optional) indent spaces and a newline.
	-- Since lines do not nest, this function is much simpler.
	-- So much simpler that it always returns one result:
	-- an unbalanced indent can only occur once - on the last line of a range.
	local function unbalanced_indent(range)
		local eol = eol_at(range.finish)
		if eol == range.finish then return end
		local bol = bol_at(range.finish)
		local _, parent = walker.sexp_at({start = bol, finish = bol})
		if not parent.is_list then return end
		local start = math.max(bol - 1, range.start)
		local nxt = parent.after(bol, startof)
		local finish = nxt and nxt.start or parent.finish
		if contains(range, finish) then
			return {start = start, finish = finish}
		end
		if within(range.finish, start, finish) then
			return {start = start, finish = math.min(finish, range.finish)}
		end
		-- TODO: Backspace at dangling ) doesn't gather it to the next line anymore.
		-- this is because of delete_whitespace_only doesn't work in this situation. FIX IT.
		-- Don't forget to simplify delete_whitespace_only() in the end.
	end

	local function pick_out(range, pos, action)
		local ndeleted = 0
		if range.start == range.finish then return ndeleted end
		local sexps = parser.tree
		local skips = sexps.unbalanced_delimiters(range)
		-- handle splice and kill-splice of forms and strings:
		if #skips == 1 then
			local sexp = walker.sexp_at(skips[1], true)
			local backward_splice = skips[1].opening and pos >= sexp.start + (sexp.p and #sexp.p or 0) + #sexp.d
				and range.start >= sexp.start
			local forward_splice = skips[1].closing and pos <= sexp.finish + 1 - #parser.opposite[sexp.d]
				and range.finish <= sexp.finish + 1
			if backward_splice or forward_splice then
				return splice(backward_splice and range.finish or range.start, sexps, sexp, backward_splice, action)
			end
		end
		local node, parent = walker.sexp_at({start = range.finish, finish = range.finish})
		-- if the range ends with a line comment, don't delete its closing newline:
		local drop_eol = action.kill and node and
			node.finish + 1 == range.finish and node.is_line_comment
		local par = big_enough_parent(range.start, range.finish)
		local operator_changed = par[1] and par[1].finish >= range.start
		local whitespace_only = delete_whitespace_only(range, pos)
		local refmt = #skips == 0 and whitespace_only or operator_changed and 0
		local indent_skip = not whitespace_only and unbalanced_indent(range)
		if indent_skip then
			table.insert(skips, indent_skip)
		end
		table.sort(skips, function(a, b) return a.start < b.start end)
		table.insert(skips, {start = range.finish - (drop_eol and 1 or 0)})
		table.insert(skips, 1, {finish = range.start})
		ndeleted = ndeleted + (drop_eol and 1 or 0)
		for i = #skips - 1, 1, -1 do
			local region = {start = skips[i].finish, finish = skips[i + 1].start}
			-- TODO: a| (word) should also preserve the space
			-- maybe add .sexp from unbalanced_delimiters. but then it would be
			-- convenient to have .prev and .next too.
			-- maybe move this block out of this loop, and adjust the skip regions instead
			if skips[i].closing and skips[i + 1].opening then
				-- leave out some of the space between adjacent lists
				local _, rparent = walker.sexp_at(region)
				local nxt = rparent.after(region.start, startof)
				region.start = nxt and nxt.start or region.start
			end
			if action then
				-- TODO: region sometimes has start > finish
				action.func(region)
				ndeleted = ndeleted + (region.finish - region.start)
			end
		end
		-- if parent[#parent + 1] is nil, we are at EOF
		if ndeleted > 0 and (not parent.is_root or parent.is_parsed(range.start) or parent[#parent + 1]) then
			sexps.rewind(range.start)
		end
		return ndeleted - (refmt or 0), nil, nil, nil, refmt
	end

	local function raise_sexp(range, pos)
		local sexp, parent = walker.sexp_at(range, true)
		if sexp and parent and parent.is_list then
			delete(sexp.finish + 1, parent.finish - sexp.finish)
			delete(parent.start, sexp.start - parent.start)
			parser.tree.rewind(parent.start)
			range.start = parent.start + pos - sexp.start
			range.finish = range.start
			local _, nodes = walker.sexp_path(range)
			local grandparent = nodes[#nodes - 2]
			return grandparent and refmt_at(grandparent, range) or range.start
		end
	end

	local function slurp_sexp(range, forward)
		local _, parent = walker.sexp_at(range, true)
		local seeker = forward and walker.finish_after or walker.start_before
		if not parent or not parent.is_list then return range.start end
		local r = {start = parent.start, finish = parent.finish + 1}
		local newpos = seeker(r, is_comment)
		if not newpos then return range.start end
		local opening = (parent.p or "")..parent.d
		local closing = parser.opposite[parent.d]
		local delimiter = forward and closing or opening
		if forward then
			write(newpos, delimiter)
		end
		delete(forward and parent.finish or parent.start, #delimiter)
		if not forward then
			write(newpos, delimiter)
		end
		parser.tree.rewind(math.min(parent.start, newpos))
		return refmt_at(big_enough_parent(newpos, range.start), range)
	end

	local function barf_sexp(range, forward)
		local _, parent = walker.sexp_at(range, true)
		local seeker = forward and walker.finish_before or walker.start_after
		-- TODO: barfing out of strings requires calling the parser on them
		if not parent or not parent.is_list or parent.is_empty then return range.start end
		local opening = (parent.p or "")..parent.d
		local pstart = parent.start + #opening
		local r = {start = forward and parent.finish - 1 or pstart, finish = forward and parent.finish or pstart + 1}
		local newpos = seeker(r, is_comment) or forward and pstart or parent.finish
		local closing = parser.opposite[parent.d]
		local delimiter = forward and closing or opening
		if not forward then
			write(newpos, delimiter)
		end
		delete(forward and parent.finish or parent.start, #delimiter)
		if forward then
			write(newpos, delimiter)
		end
		parser.tree.rewind(math.min(parent.start, newpos))
		local drag = forward and (newpos < range.finish) or not forward and (newpos > range.start)
		local keep_inside = forward and #parent > 1 and range.finish > range.start and 1 or 0
		newpos = drag and newpos - keep_inside or range.start
		local rangeng = {start = newpos, finish = newpos}
		return refmt_at(big_enough_parent(newpos, parent.finish + 1), rangeng)
	end

	local function splice_anylist(range, _, no_refmt)
		local _, parent = walker.sexp_at(range)
		if not parent or not parent.d then return end
		local opening = (parent.p or "")..parent.d
		local closing = parser.opposite[parent.d]
		local finish = parent.finish + 1 - #closing
		if not parent.is_line_comment then
			delete(finish, #closing)
		end
		-- TODO: (un)escape special characters, if necessary
		delete(parent.start, parent.is_empty and (finish - parent.start) or #opening)
		parser.tree.rewind(parent.start)
		range.start = parent.start
		range.finish = range.start
		local _, parentng = walker.sexp_at(range, true)
		return not no_refmt and refmt_at(parentng, range) or range.start
	end

	local function rewrap(parent, kind)
		local pstart = parent.start + #((parent.p or "")..parent.d) - 1
		delete(parent.finish, 1)
		write(parent.finish, parser.opposite[kind])
		delete(pstart, #parent.d)
		write(pstart, kind)
		parser.tree.rewind(parent.start)
	end

	local function cycle_wrap(range, pos)
		local _, parent = walker.sexp_at(range)
		if not parent or not parent.is_list then return end
		local next_kind = {["("] = "[", ["["] = "{", ["{"] = "("}
		rewrap(parent, next_kind[parent.d])
		return refmt_at(parent, range) or pos
	end

	local function split_anylist(range)
		local _, parent = walker.sexp_at(range)
		if not (parent and parent.d) then return end
		local new_finish, new_start
		if parent.is_list then
			local prev = parent.before(range.start, finishof, is_comment)
			new_finish = prev and prev.finish + 1
			-- XXX: do not skip comments here, so they end up in the second list
			-- and are not separated from their target expression:
			local nxt = new_finish and parent.after(new_finish, startof)
			new_start = nxt and nxt.start
		else
			new_start = range.start
			new_finish = range.start
		end
		if not (new_start and new_finish) then return end
		local opening = (parent.p or "")..parent.d
		local closing = parser.opposite[parent.d]
		write(new_start, opening)
		local sep = parent.is_line_comment and ""  -- line comments already have a separator
			or new_finish == new_start and " "  -- only add a separator if there was none before
			or ""
		write(new_finish, closing..sep)
		parser.tree.rewind(parent.start)
		range.start = new_start + (parent.is_list and 0 or #opening + #closing)
		range.finish = range.start
		local _, nodes = walker.sexp_path(range)
		local parentng, grandparent = nodes[#nodes - 1], nodes[#nodes - 2]
		local scope = parentng and not parentng.is_root and parentng or grandparent
		return refmt_at(scope, range)
	end

	local function join_anylists(range)
		local node, parent = walker.sexp_at(range, true)
		local first = node and node.finish + 1 == range.start and node or parent.before(range.start, finishof)
		local second = first ~= node and node or parent.after(range.start, startof)
		if not (first and second and first.d and
			-- don't join line comments to margin comments:
			(not first.is_line_comment or first.indent and second.indent) and
			(first.d == second.d or
				-- join line comments even when their delimiters differ slightly
				-- (different number of semicolons, existence/lack of a space after them)
				parser.opposite[first.d] == parser.opposite[second.d])) then
			return
		end
		local opening = (second.p or "")..second.d
		local closing = parser.opposite[first.d]
		local pos
		if not first.is_list then
			pos = first.finish + 1 - #closing
			delete(pos, second.start + #opening - pos)
		else
			delete(second.start, #opening)
			delete(first.finish, #closing)
			pos = second.start - #closing
		end
		parser.tree.rewind(first.start)
		range.start = pos
		range.finish = range.start
		local _, nodes = walker.sexp_path(range)
		local parentng, grandparent = nodes[#nodes - 1], nodes[#nodes - 2]
		local scope = parentng and not parentng.is_root and parentng or grandparent
		return refmt_at(scope, range)
	end

	local function delete_splicing(range, pos, splicing, delete_and_yank)
		local action = {kill = true, wrap = splicing, splice = splicing, func = delete_and_yank}
		local sexp, parent, n = walker.sexp_at(range)
		local nxt = n and parent[n + 1]
		local prev = n and parent[n - 1]
		range = extend_overlap(range)
		local ndeleted, spliced, opening, closing = pick_out(range, pos, action)
		local inner_list_len = spliced and sexp.finish - sexp.start + 1 - #opening - #closing
		local range_len = range.finish - range.start
		local backwards = pos == range.finish
		local whole_object = sexp and
			(sexp.start == range.start and sexp.finish + 1 == range.finish
			or spliced and inner_list_len <= range_len - (backwards and #opening or #closing))
		local in_head_atom = sexp and not sexp.d and n == 1 and #parent > 1
		local in_whitespace = not sexp
		if whole_object or spliced or in_whitespace or in_head_atom then
			local cur = (whole_object or spliced) and (prev and prev.finish or sexp.start) or range.start
			-- if parent[#parent + 1] is nil, we are at EOF
			if not parent.is_root or parent.is_parsed(cur) or parent[#parent + 1] then
				parser.tree.rewind(cur)
			end
			-- make sure the cursor is not left in whitespace:
			if whole_object and nxt then
				cur = nxt.start - range_len - (spliced and (backwards and #closing or #opening) or 0)
			elseif whole_object and prev then
				cur = prev.finish
			elseif whole_object and parent.d and not parent.is_list then
				local _, parentng = walker.sexp_at({start = cur, finish = cur})
				local trailing = parentng.spaces_after(sexp.start)
				if trailing then
					delete(sexp.start, trailing - sexp.start)
					parser.tree.rewind(sexp.start)
					local c = parser.opposite[parentng.d]
					if trailing == parentng.finish + 1 - #c then
						local leading = parentng.spaces_before(sexp.start)
						-- keep the cursor inside the parent
						cur = (leading or cur) - 1
					end
				else
					local leading = parentng.spaces_before(sexp.start)
					if leading then
						delete(leading, sexp.start - leading)
						parser.tree.rewind(leading)
						-- keep the cursor inside the parent
						cur = leading - 1
					end
				end
			elseif spliced then
				cur = (backwards and sexp.start or (nxt and nxt.start - range_len or range.start) - ndeleted)
			end
			local r = {start = cur, finish = cur + 1}
			local _, parentng = walker.sexp_at(r, true)
			return refmt_at(parentng, r)
		end
		return not backwards and ndeleted <= 0 and range.finish - ndeleted
			or backwards and ndeleted <= 0 and range.start + ndeleted
			or range.start
	end

	local function transpose(range, first, second)
		if not (first and second) then return end
		local copy1 = first.text
		local copy2 = second.text
		delete(second.start, second.finish + 1 - second.start)
		write(second.start, copy1)
		delete(first.start, first.finish + 1 - first.start)
		write(first.start, copy2)
		parser.tree.rewind(first.start)
		range.start = second.finish + 1
		range.finish = range.start
		return refmt_at(big_enough_parent(first.start, second.finish), range)
	end

	local function transpose_sexps(range)
		local node, parent = walker.sexp_at(range, true)
		local first = node and node.finish + 1 == range.start and node or parent.before(range.start, finishof)
		local second = first ~= node and node or parent.after(range.start, startof)
		return transpose(range, first, second)
	end

	local function transpose_words(range)
		local first, second, _
		first = walker.sexp_at(range)
		if not first or first.d or first.start == range.finish then
			_, first = walker.start_float_before(range)
		end
		if first then
			_, second = walker.finish_float_after({start = first.finish + 1, finish = first.finish + 1})
		end
		return transpose(range, first, second)
	end

	local function transpose_chars(range)
		local node, parent, i = walker.sexp_at(range)
		local nxt = i and parent[i + 1]
		local pstart = not parent.is_root and parent.start + (parent.p and #parent.p or 0) + #parent.d
		local pfinish = not parent.is_root and parent.finish
		local npref = node and node.p and node.start + #node.p
		-- only allow transposing while inside atoms/words and prefixes
		if node and ((npref and (range.start <= npref and range.start > node.start) or
			node.d and range.start > node.finish + 1) or not node.d and (not pstart or range.start > pstart)) then
			local start = range.start -
				((range.start == pfinish or range.start == npref or
				range.start == node.finish + 1 and (parent.is_list or parent.is_root) and (not nxt or nxt.indent)) and 1 or 0)
			local str_start = start - node.start + 1
			local char = node.text:sub(str_start, str_start)
			delete(start, 1)
			write(start - 1, #char > 0 and char or " ")
			parser.tree.rewind(start)
			local in_head_atom = i == 1 and #parent > 1
			return parent.is_list and in_head_atom and refmt_at(parent, {start = start + 1, finish = start + 1}) or start + 1
		end
	end

	local function _join_or_splice(parent, n, range, pos)
		local sexp = parent[n]
		local nxt = parent[n + 1]
		local is_last = not nxt or not nxt.is_line_comment
		local newpos = not (is_last and sexp.is_empty) and join_anylists(range)
		if not newpos and sexp.is_empty then
			newpos = splice_anylist({start = pos, finish = pos}, nil, true)
		end
		return newpos or pos
	end

	local function delete_nonsplicing(range, pos, delete_maybe_yank)
		local action = {kill = true, wrap = false, splice = false, func = delete_maybe_yank}
		range = extend_overlap(range)
		local ndeleted, spliced, opening, _, refmt = pick_out(range, pos, action)
		local backwards = pos == range.finish
		if opening then
			if spliced then
				return pos - ndeleted
			else
				if ndeleted == 0 then
					local closing = parser.opposite[opening] -- XXX: why don't I use the pick_out return value?
					if closing == "\n" then
						local sexp, parent, n = walker.sexp_at(range)
						if pos == sexp.start + #sexp.d and backwards then
							return _join_or_splice(parent, n, range, pos)
						elseif pos == sexp.finish then
							local r = {start = pos + #closing, finish = pos + #closing}
							return _join_or_splice(parent, n, r, pos)
						end
					end
				end
				return backwards and (range.finish - ndeleted) or range.start
			end
		else
			local newpos = backwards and range.start + (ndeleted <= 0 and ndeleted or 0) or (range.finish - ndeleted)
			if refmt then
				local r = {start = newpos, finish = newpos}
				return refmt_at(big_enough_parent(range.start, range.finish - ndeleted), r, true)
			end
			return newpos
		end
	end

	local function insert_pair(range, delimiter, auto_square)
		local indices, nodes = walker.sexp_path(range)
		local sexp = nodes[#nodes]
		local right_after_prefix = sexp and sexp.p and range.start == sexp.start + #sexp.p
		-- XXX: here I assume that # is a valid prefix for the dialect
		local mb_closing = (delimiter == "|") and right_after_prefix and parser.opposite[sexp.p .. delimiter]
		local closing = mb_closing or parser.opposite[delimiter]
		if delimiter == '"' and sexp and sexp.is_string
			and range.start > sexp.start
			and range.finish <= sexp.finish then
				delimiter, closing = '\\'..delimiter, '\\'..closing
		elseif squarewords and not right_after_prefix
			and (auto_square or squarewords.not_optional)
			and not adjust_bracket_p(indices, nodes, range) then
				delimiter, closing = "[", "]"
		end
		write(range.finish, closing)
		write(range.start, delimiter)
		if right_after_prefix or parser.tree.is_parsed(range.start) then
			parser.tree.rewind(right_after_prefix and sexp.start or range.start)
		end
		return range.start + #delimiter
	end

	local function make_wrap(opening)
		return parser.opposite[opening] and function(range, pos, auto_square)
			local newpos
			local function _wrap(r)
				if r.finish <= r.start then return end
				newpos = insert_pair(r, opening, auto_square)
			end
			local action = {kill = false, wrap = false, splice = false, func = _wrap}
			pick_out(range, pos, action)
			-- XXX: here I assume that #| is the only multi-char delimitier,
			-- and that it means block comment
			local bump = newpos and #opening == 1 and (newpos - range.start) or 0
			local rangeng = {start = range.start + bump, finish = range.finish + bump}
			local _, parentng = walker.sexp_at(range)  -- TODO: range.finish is wrong. use sexp_path and big_enough_parent
			return refmt_at(parentng, rangeng)
		end
	end

	local function convolute_lists(range)
		local path, nodes = walker.sexp_path(range)
		local parent = nodes[#nodes - 1]
		local grandparent = nodes[#nodes - 2]
		if not grandparent or not grandparent.is_list then return end
		local gprange = { start = grandparent.start, finish = grandparent.finish + 1 }
		local pprefix = parent.p
		local prefix_len = pprefix and #pprefix or 0
		insert_pair(gprange, parent.d)
		local rangeng = { start = range.start + #parent.d, finish = range.finish + #parent.d }
		path, nodes = walker.sexp_path(rangeng)
		local sexp = nodes[#nodes]
		parent = nodes[#nodes - 1]
		grandparent = nodes[#nodes - 2]
		delete(parent.finish, #parser.opposite[parent.d])
		local head = parent.text:sub(prefix_len + #parent.d + 1, (sexp and sexp.start or rangeng.start) - parent.start)
		delete(parent.start, (sexp and sexp.start or rangeng.start) - parent.start)
		write(grandparent.start, head)
		if pprefix then
			write(gprange.start, pprefix)
			gprange.finish = gprange.finish - #pprefix
		end
		parser.tree.rewind(gprange.start)
		return refmt_at(gprange, {start = range.start, finish = range.start}) or range.start
	end

	local function newline(parent, range)
		local line_comment = parent.is_line_comment
		-- do not autoextend margin comments:
		if line_comment and range.start < parent.finish + (parent.indent and 1 or 0)  then
			local newpos = split_anylist(range)
			if newpos then
				return newpos
			end
		end
		if not parent.is_list and not line_comment then
			-- if parent[#parent + 1] is nil, we are at EOF
			if not parent.is_root or parent.is_parsed(range.start) or parent[#parent + 1] then
				parser.tree.rewind(parent.start or range.start)
			end
			local newpos = range.start
			write(newpos, "\n")
			return newpos + 1
		end
		if not parent.is_list then
			local _
			_, parent = walker.sexp_at(parent, true)
		end
		local last_nonblank_in_list = not parent.is_empty and parent[#parent].finish - (line_comment and 1 or 0)
		local nxt = parent.after(range.start, startof)
		local last_on_line = range.start == eol_at(range.start)
		local margin = nxt and not nxt.indent and nxt.is_line_comment and nxt.finish
		local after_last = last_nonblank_in_list and range.start > last_nonblank_in_list
		local in_indent = nxt and nxt.indent and range.start <= nxt.start and range.start >= (nxt.start - nxt.indent)
		local placeholder = "asdf"
		local newpos = margin or range.start
		if not parent.is_empty then
			if in_indent then
				write(newpos, (placeholder.."\n"))
			else
				write(newpos, "\n"..((after_last or last_on_line or margin) and placeholder or ""))
			end
		end
		parser.tree.rewind(parent.start or newpos)
		-- move the cursor onto the placeholder, so refmt_at can restore the position:
		local r = {start = newpos, finish = newpos}
		newpos = walker.start_after(r) or newpos
		local rangeng = {start = newpos, finish = newpos}
		newpos = refmt_at(parent, rangeng)
		local _, parentng = walker.sexp_at({start = newpos, finish = newpos}, true)
		if after_last then
			local autoindent = parentng[#parentng].indent
			write(parentng.finish, "\n"..string.rep(" ", autoindent or 0))
		end
		if after_last or last_on_line or margin or in_indent then
			delete(newpos, #placeholder)
		end
		parser.tree.rewind(parentng.start or newpos)
		return newpos
	end

	local function close_and_newline(range, closing)
		local opening = closing and parser.opposite[closing]
		local parent = walker.list_at(range, opening)
		if parent then
			local has_eol = parent.is_line_comment
			local r = {start = parent.finish, finish = parent.finish}
			local newpos = refmt_at(parent, r)
			r = {start = newpos + (has_eol and 0 or 1), finish = newpos + (has_eol and 0 or 1)}
			local list, parentng = walker.sexp_at(r)
			newpos = list and list.finish + 1 or r.start
			newpos = newline(parentng, {start = newpos, finish = newpos})
			if not newpos then
				newpos = r.finish + 1
				write(newpos, "\n")
				parser.tree.rewind(parentng.start or list.start)
			end
			return newpos
		elseif closing == "\n" then
			local eol = eol_at(range.start)
			local _, parentng = walker.sexp_at({start = eol, finish = eol})
			local newpos = newline(parentng, {start = eol, finish = eol})
			return newpos
		end
		return range.start
	end

	local function join_line(_, pos)
		local eol = eol_at(pos)
		local r = {start = eol, finish = eol + 1}
		return delete_nonsplicing(r, r.start, delete)
	end

	local block_comment_start
	for o in pairs(parser.opposite) do
		if #o > 1 then
			block_comment_start = o
			break
		end
	end

	return {
		delete_splicing = delete_splicing,
		delete_nonsplicing = delete_nonsplicing,
		refmt_at = refmt_at,
		pick_out = pick_out,
		raise_sexp = raise_sexp,
		slurp_sexp = slurp_sexp,
		barf_sexp = barf_sexp,
		splice_anylist = splice_anylist,
		wrap_round = make_wrap"(",
		wrap_square = make_wrap"[",
		wrap_curly = make_wrap"{",
		wrap_doublequote = make_wrap'"',
		wrap_comment = make_wrap(block_comment_start),
		insert_pair = insert_pair,
		newline = newline,
		join_line = join_line,
		close_and_newline = close_and_newline,
		cycle_wrap = cycle_wrap,
		split_anylist = split_anylist,
		join_anylists = join_anylists,
		convolute_lists = convolute_lists,
		transpose_sexps = transpose_sexps,
		transpose_words = transpose_words,
		transpose_chars = transpose_chars,
	}
end

return M
