-- SPDX-License-Identifier: GPL-3.0-or-later
-- © 2020 Georgi Kirilov

local l = require'lpeg'
local P, S, Cc = l.P, l.S, l.Cc

local lispwords = {
	-- If none of the string keys matched, this pattern is attempted
	[0] = ('def' *
		('n' * P'-'^-1 + 'interface' + 'macro' + 'method' + 'multi' +
		'once' + 'protocol' + 'record' + 'struct' + 'type')^-1 * -P(1)) * Cc(1),

	['comment'] = -1,
	['cond'] = -1,
	['delay'] = -1,
	['do'] = -1,
	['finally'] = -1,
	['future'] = -1,
	['try'] = -1,

	['binding'] = 1,
	['case'] = 1,
	['cond->'] = 1,
	['cond->>'] = 1,
	['dorun'] = 1,
	['doseq'] = 1,
	['dotimes'] = 1,
	['doto'] = 1,
	['extend'] = 1,
	['extend-protocol'] = 1,
	['extend-type'] = 1,
	['fn'] = 1,
	['for'] = 1,
	['if'] = 1,
	['if-let'] = 1,
	['if-not'] = 1,
	['if-some'] = 1,
	[':import'] = 1,
	['iterate'] = 1,
	['let'] = 1,
	['letfn'] = 1,
	['locking'] = 1,
	['loop'] = 1,
	['ns'] = 1,
	['proxy'] = 1,
	['reify'] = 1,
	['some->'] = 1,
	['some->>'] = 1,
	['specify!'] = 1,
	['specify'] = 1,
	['this-as'] = 1,
	['when'] = 1,
	['when-first'] = 1,
	['when-let'] = 1,
	['when-not'] = 1,
	['when-some'] = 1,
	['while'] = 1,
	['with-local-vars'] = 1,

	['as->'] = 2,
	['catch'] = 2,
	['condp'] = 2,
}

local squarewords = {
	not_optional = true,

	['let'] = 1,

	['fn'] = 2,

	['defn'] = {false, 1},
}

-- XXX: since it doesn't seem possible to tell when one is about to use fn/defn with single- or multi-arity,
-- the rules above make the assumption that you use fn with single-arity, and defn - with multi-arity.

local macro_prefix =
	P'#' * ('?' * P'@'^-1 + '_')^-1 +
	S"^@`'"

local weak_prefix = P'#'

local delimiters = {
	['('] = ')',
	['{'] = '}',
	['['] = ']',
	['"'] = '"',
	[';'] = '\n',
}

return {
	prefix = macro_prefix,
	weak_prefix = weak_prefix,
	opposite = delimiters,
	lispwords = lispwords,
	squarewords = squarewords,
}
